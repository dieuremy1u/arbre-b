# Projet Arbre B+ #

Projet L3 Miage : Modélisation BDD

### Equipe ###

* Guylan Dieu-Remy
* Thomas Ehrhard

### Exécution ###

Pour démarrer l'application : 

1. Compiler les fichiers .java  
2. Exécuter la classe Main  
3. Suivre les instructions affichées en ligne de commande  

### Détail du menu et du fonctionnement de l'application ###

* Lors du lancement de l'application on demande d'abord de saisir la valeur de n (taille maximale des noeuds)
* Le programme initialise ensuite les arbres en fonction de n et des données contenues dans le fichier donnees.txt
* Chaque arbre (correspondant à un index) s'affiche
* Un menu propose alors différentes actions :

	#### Affichage ####
	* Entrer "p" pour afficher tous les arbres
	* Chaque arbre s'affiche
	
	#### Ajout ####
	* Entrer "a" pour ajouter un enregistrement
	* Pour chaque colonne, on demande de rentrer des valeurs selon le type de la clé (entier ou chaine)
	* L'enregistrement est ajouté à chaque arbre
	* Chaque arbre s'affiche avec ses modifications
	
	#### Suppression ####
	* Entrer "s" pour supprimer un enregistrement
	* On demande de saisir une clé
	* Si la clé est trouvée dans un des arbres, l'enregistrement est supprimé de chaque arbre, et chaque arbre s'affiche avec ses modifications, sinon, on indique que la clé est introuvable
	
	#### Recherche simple ####
	* Entrer "r" pour effectuer une recherche simple
	* On demande le numéro de l'index sur lequel effectuer la recherche
	* On demande de saisir la clé à rechercher (entier ou chaine selon l'index)
	* Le parcours de l'arbre lors de la recherche s'affiche
	* Si la clé est trouvée, l'enregistrement est affiché, sinon, on indique que la clé est introuvable
	
	#### Recherche par intervalle ####
	* Entrer "rr" pour effectuer une recherche par intervalle
	* On demande le numéro d'index sur lequel effectuer la recherche
	* On demande de saisir la clé de départ, puis la clé de fin (entier ou chaine selon l'index)
	* Le parcours de l'arbre lors de la recherche s'affiche
	* Si une clé est trouvée dans l'intervalle, les enregistrements s'affichent les uns après les autres, sinon, on indique qu'aucune clé n'est trouvée dans l'intervalle donné
	
	#### Quitter ####
	* Entrer "q" pour quitter le programme

### Statégies abordées ###

* Les stratégies choisies lors de l'ajout et de la suppression :

	#### Ajout ####
	* Les pointeurs des noeuds pointent vers les clés < à gauche et >= à droite
	* Lors du split d'un noeud avec un nombre de valeurs impaire, la valeur médianne va vers le nouveau noeud (à droite)
	
	#### Suppression ####
	* La suppression s'effectue selon la méthode "full" : les clés supprimées/mergées dans les feuilles sont mises à jour dans les noeuds parents
	* Lors du merge, on choisi par défaut de merger avec le noeud de droite. Si le noeud n'a pas de voisin à droite, alors il merge avec le voisin de gauche

### Informations complémentaires ###

* Le code source est commenté et javadoqué
* Pour s'exécuter correctement, le programme Main a par défaut besoin du fichier donnees.txt (à sa source) pour initialiser les arbres. Ce fichier peut être modifié pour initialiser différemment les arbres
* Le programme est conçu pour prendre en compte un nombre de colonnes modulable (c'est à dire des enregistrements de taille modulable). Pour cela, on considère que les valeurs de chaque colonne sont des **clés** et doivent donc être **uniques**. C'est à dire qu'à chaque colonne correspondra un index (et donc un arbre)