import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Noeud qui contient un certain nombre de clés et valeurs
 * 
 * @author Guylan Dieu-Remy, Thomas Ehrhard
 *
 * @param <E> type des clés du noeud
 */
public class Noeud<E extends Comparable<E>> {
	
	/**
	 * Compteur d'id
	 */
	private static int compteur = 0;
	
	/**
	 * Id du noeud
	 */
	protected int id;
	
	/**
	 * Vrai si le noeud est une feuille, faux sinon
	 */
	protected boolean estFeuille;
	
	/**
	 * Vrai si le noeud est une racine, faux sinon
	 */
	protected boolean estRacine;
	
	/**
	 * Vrai si le noeud est intermédiaire, faux sinon
	 */
	protected boolean estIntermediaire;
	
	/**
	 * Ensemble de valeurs (= clés) que contient le noeud
	 */
	protected TreeSet<E> valeurs;
	
	/**
	 * Ensemble de clés pointant vers vers les données de l'enregistrement. Null si le noeud n'est pas une feuille
	 */
	protected TreeMap<E, Object[]> donneesLiees;
	
	/**
	 * Liste de pointeurs vers les noeuds enfants
	 */
	protected ArrayList<Noeud<E>> pointeurs;
	
	/**
	 * Pointeur vers la feuille de droite. Null si le noeud n'est pas une feuille ou s'il est la dernière feuille
	 */
	protected Noeud<E> feuilleDroite;
	
	/**
	 * Pointeur vers le noeud parent. Null si le noeud est une racine
	 */
	protected Noeud<E> parent;
	
	/**
	 * Niveau du noeud dans son arbre
	 */
	protected int niveau;

	
	/**
	 * Constructeur de la racine vide qui initilise l'arbre
	 */
	public Noeud() {
		this.id = Noeud.compteur++;
		this.niveau = 0;
		this.estRacine = true;
		this.estIntermediaire = false;
		this.estFeuille = true;
		this.valeurs = new TreeSet<E>();
		this.parent = null;
		this.pointeurs = new ArrayList<Noeud<E>>();
		this.donneesLiees = new TreeMap<E, Object[]>();
	}

	/**
	 * Constructeur d'une nouvelle racine
	 */
	public Noeud(TreeSet<E> valeurs, ArrayList<Noeud<E>> pointeurs) {
		this.id = Noeud.compteur++;
		this.niveau = 0;
		this.estRacine = true;
		this.estIntermediaire = false;
		this.estFeuille = false;
		this.valeurs = valeurs;
		this.parent = null;
		this.pointeurs = pointeurs;
		this.donneesLiees = null;
		this.trierPointeurs();
	}

	/**
	 * Constructeur d'un nouveau noeud intermédaire
	 */
	public Noeud(int niveau, TreeSet<E> valeurs, ArrayList<Noeud<E>> pointeurs, Noeud<E> parent) {
		this.id = Noeud.compteur++;
		this.niveau = niveau;
		this.estRacine = false;
		this.estIntermediaire = true;
		this.estFeuille = false;
		this.valeurs = valeurs;
		this.parent = parent;
		this.pointeurs = pointeurs;
		this.donneesLiees = null;
		this.trierPointeurs();
	}

	/**
	 * Constructeur d'un nouveau noeud feuille
	 */
	public Noeud(int niveau, TreeSet<E> valeurs, ArrayList<Noeud<E>> pointeurs, Noeud<E> parent, TreeMap<E, Object[]> donneeLiees) {
		this.id = Noeud.compteur++;
		this.niveau = niveau;
		this.estRacine = false;
		this.estIntermediaire = false;
		this.estFeuille = true;
		this.valeurs = valeurs;
		this.parent = parent;
		this.pointeurs = pointeurs;
		this.donneesLiees = donneeLiees;
		this.trierPointeurs();
	}

	/**
	 * Ajoute un enregistrement au noeud
	 * @param key clé de l'enregistrement
	 * @param values données de l'enregistrement
	 */
	public void ajouterValeur(E key, Object[] values) {
		this.valeurs.add(key);
		if(this.estFeuille)
			this.donneesLiees.put(key, values);
	}

	/**
	 * Supprime un enregistrement du noeud
	 * @param key clé de l'enregsitrement
	 * @return vrai si la valeur a supprimer est la première valeur de son noeud, faux sinon
	 */
	public boolean supprimerValeur(E key) {
		boolean res = false;
		if(key == this.valeurs.first())
			res = true;
		if(this.estFeuille) {
			this.donneesLiees.remove(key);
		}
		this.valeurs.remove(key);
		return res; // utile pour la méthode full
	}

	/**
	 * Ajoute un pointeur au noeud
	 * @param n Pointeur à ajouter au noeud
	 */
	public void ajouterPointeur(Noeud<E> n) {
		this.pointeurs.add(n);
		this.trierPointeurs();
	}

	/**
	 * Supprime un pointeur au noeud
	 * @param n Pointeur à supprimer du noeud
	 */
	public void supprimerPointeur(Noeud<E> n) {
		this.pointeurs.remove(n);
		this.trierPointeurs();
	}

	/**
	 * Trie les pointeurs du noeud
	 */
	private void trierPointeurs() {
		Collections.sort(this.pointeurs, new Comparator<Noeud<E>>() {
			@Override
			public int compare(Noeud<E> n1, Noeud<E> n2) {
				if(n1.valeurs.first().compareTo(n2.valeurs.first()) < 0)
					return -1;
				else if(n1.valeurs.first().compareTo(n2.valeurs.first()) > 0)
					return 1;
				return 0;
			}
		});
	}

	/**
	 * Donne le voisin de droite du noeud
	 * @return noeud voisin
	 */
	public Noeud<E> getVoisinDroite() {
		if(this.estRacine) return null; else {
			int i = this.parent.pointeurs.indexOf(this) + 1;
			if(i < this.parent.pointeurs.size())
				return this.parent.pointeurs.get(i);
			return null;
		}
	}

	/**
	 * Donne le voisin de gauche du noeud
	 * @return noeud voisin
	 */
	public Noeud<E> getVoisinGauche() {
		if(this.estRacine) return null; else {
			int i = this.parent.pointeurs.indexOf(this) - 1;
			if(i >= 0)
				return this.parent.pointeurs.get(i);
			return null;
		}
	}

	/**
	 * Affichage par défaut d'un noeud
	 * @return String chaine de caractère représentant un noeud en ligne de commande
	 */
	public String toString() {
		StringBuffer res = new StringBuffer();
		StringBuffer tabulation = new StringBuffer();
		for(int i = 0; i < this.niveau; i++)
			tabulation.append("       ");
		StringBuffer strTirets = new StringBuffer(tabulation + " ");
		StringBuffer strValeurs = new StringBuffer(tabulation + "| ");
		int s = this.valeurs.size() - 1;
		int c = 0;
		for (E valeur : this.valeurs) {
			strTirets.append("--");
			for (int i = 0; i < valeur.toString().length(); i++) {
				strTirets.append("-");
			}
			if(c++ < s) strTirets.append("-");
			strValeurs.append(valeur.toString() + " | ");
		}
		res.append(strTirets + "\n" + strValeurs + this.getStringType() + this.getStringId() + this.getStringParent() + this.getStringFeuilleDroite() + "\n" + strTirets + "\n");
		if(this.donneesLiees != null) {
			tabulation.append("  ");
			res.append(tabulation);
			int i = 0;
			for (Entry<E, Object[]> entry : this.donneesLiees.entrySet()) {
				for (Object o : entry.getValue())
					res.append(entry.getKey() + " -> " + o.toString() + "; ");
				res.append("\n");
				if(i++ < this.donneesLiees.size()-1) res.append(tabulation);
			}
		}
		return res.toString();
	}

	/**
	 * Affichage du type du noeud
	 * @return affichage du type du noeud
	 */
	private String getStringType() {
		StringBuffer str = new StringBuffer();
		if(this.estRacine) str.append("racine ");
		if(this.estIntermediaire) str.append("intermédiaire ");
		if(this.estFeuille) str.append("feuille ");
		return str.toString();			
	}

	/**
	 * Affichage de l'id du noeud
	 * @return affichage de l'id du noeud
	 */
	private String getStringId() {
		return "| Id : #" + this.id;
	}

	/**
	 * Affichage de l'id du parent du noeud
	 * @return affichage de l'id du parent du noeud
	 */
	private String getStringParent() {
		if(this.parent != null)
			return " | Id parent : #" + this.parent.id;
		return "";
	}

	/**
	 * Affichage de l'id de la feuille de droite du noeud
	 * @return affichage de l'id de la feuille de droite du noeud
	 */
	private String getStringFeuilleDroite() {
		if(this.feuilleDroite != null) {
			return " | Id feuille de droite : #" + this.feuilleDroite.id;
		}
		return "";
	}

}
