import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * Main qui met en application les arbres B+, proposant des options d'affichage, d'ajout, de suppression, et de recherches
 * 
 * @author Guylan Dieu-Remy, Thomas Ehrhard
 *
 */
public class Main {

	/**
	 * Construit et initialise des arbres depuis des données
	 * @param n taille des noeuds des arbres
	 * @param donnees données d'initilisation
	 * @param nbIndex nombre d'index à créer (correspondant à autant d'arbres)
	 * @return liste des arbres qui ont été créés
	 */
	private static Arbre[] construireArbresDepuisDonnees(int n, Donnees donnees) {
		System.out.println("\nInitialisation des arbres depuis le fichier de données \"" + donnees.nomFichier + "\"");
		int nbIndex = donnees.getNbColonnes();
		Arbre[] arbres = new Arbre[nbIndex];
		for(int j = 0; j < nbIndex; j++) {
			System.out.println("\n***************************\n*   Arbre sur l'index " + (j+1) + "   *\n***************************");
			HashMap<String, Object[]> donneesIndexees = donnees.getDonneesIndexees(j);
			boolean isInteger = donnees.indexIsInterger(j);
			Arbre arbre;
			if(isInteger) 
				arbre = new Arbre<Integer>(n);
			else 
				arbre = new Arbre<String>(n);
			for (String key : donneesIndexees.keySet()) {
				if(isInteger) 
					arbre.ajouterValeur(Integer.parseInt(key), donneesIndexees.get(key));
				else
					arbre.ajouterValeur(key, donneesIndexees.get(key));
			}
			arbres[j] = arbre;
			System.out.println(arbre);
		}
		return arbres;
	}

	/**
	 * Démarre l'application
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Valeur de n : ");
		int n = new Scanner(System.in).nextInt();

		LecteurFichier lecteur = new LecteurFichier("donnees.txt");
		Donnees donnees = lecteur.getDonnees();
		Arbre[] arbres = Main.construireArbresDepuisDonnees(n, donnees);

		boolean stop = false;
		while(!stop) {
			System.out.println("Action à réaliser sur les données ? (afficher (p), ajout (a), suppression (s), recherche simple (r), recherche sur intervalle (rr), quitter (q))");
			String action = new Scanner(System.in).nextLine();

			if(action.equals("p") || action.equals("afficher")) {
				for (int i = 0; i < arbres.length; i++) {
					System.out.println("\n***************************\n*   Arbre sur l'index " + (i+1) + "   *\n***************************");
					System.out.println(arbres[i]);
				}
			}
			else if(action.equals("a") || action.equals("ajout")) {
				String[] enregistrement = new String[donnees.getNbColonnes()];
				for(int i = 0; i < donnees.getNbColonnes(); i++) {
					if(donnees.indexIsInterger(i))
						System.out.println("Veuillez renseigner un entier");
					else 
						System.out.println("Veuillez renseigner une chaine de caractère");
					String donnee = new Scanner(System.in).nextLine();
					enregistrement[i] = donnee;
				}
				for(int i = 0; i < arbres.length; i++) {
					String[] values = new String[enregistrement.length-1];
					int k = 0;
					for(int j = 0; j < enregistrement.length; j++) {
						if(i != j)
							values[k++] = enregistrement[j];
					}
					if(donnees.indexIsInterger(i))
						arbres[i].ajouterValeur(Integer.parseInt(enregistrement[i]), values);
					else
						arbres[i].ajouterValeur(enregistrement[i], values);
				}
				System.out.println("L'enregistrement à été ajouté avec succès.\n");
				for (int i = 0; i < arbres.length; i++) {
					System.out.println("\n***************************\n*   Arbre sur l'index " + (i+1) + "   *\n***************************");
					System.out.println(arbres[i]);
				}
			}
			else if(action.equals("s") || action.equals("suppression")) {
				String[] valeurs = new String[donnees.getNbColonnes() - 1];
				System.out.println("Veuillez renseigner une clé de l'enregistrement à supprimer");
				String key = new Scanner(System.in).nextLine();
				boolean trouve = false;
				int numArbreTrouve = -1;
				for(int i = 0; (i < arbres.length && !trouve); i++) {
					if(donnees.indexIsInterger(i)) {
						if((valeurs = (String[]) arbres[i].rechercherEnregistrement(Integer.parseInt(key), false)) != null) {
							trouve = true;
							numArbreTrouve = i;
							arbres[i].supprimerValeur(Integer.parseInt(key));
						}
					}
					else {
						if((valeurs = (String[]) arbres[i].rechercherEnregistrement(key, false)) != null) {
							trouve = true;
							numArbreTrouve = i;
							arbres[i].supprimerValeur(key);
						}
					}
				}
				if(trouve) {
					int j = 0;
					for (int i = 0; i < arbres.length; i++) {
						if(i != numArbreTrouve) {
							if(donnees.indexIsInterger(i)) {
								arbres[i].supprimerValeur(Integer.parseInt(valeurs[j++]));
							}
							else {
								arbres[i].supprimerValeur(valeurs[j++]);
							}
						}
					}
					System.out.println("L'enregistrement à été supprimé avec succès.\n");
					for (int i = 0; i < arbres.length; i++) {
						System.out.println("\n***************************\n*   Arbre sur l'index " + (i+1) + "   *\n***************************");
						System.out.println(arbres[i]);
					}
				}
				else
					System.out.println("La clé renseignée est introuvable.\n");
			}
			else if(action.equals("r") || action.equals("rechercher")) {
				int numArbre = 0;
				if(arbres.length > 1) {
					StringBuffer nums = new StringBuffer("(");
					for(int i = 1; i <= arbres.length; i++) {
						nums.append(i);
						if (i == arbres.length-1) nums.append(", "); else nums.append(")");
					}
					boolean correct = false;
					while(!correct) {
						System.out.println("Recherche à effectuer sur quelle index ? " + nums);
						numArbre = new Scanner(System.in).nextInt();
						if(numArbre > 0 && numArbre <= arbres.length)
							correct = true;
						else
							System.out.println("Valeur incorrecte");
					}
				} else numArbre = 1;
				numArbre--;
				boolean isInteger = donnees.indexIsInterger(numArbre);
				String type = isInteger ? "un entier" : "une chaine de caractère";
				System.out.println("Clé de l'enregistrement à rechercher ? Veuillez renseigner " + type);
				String key = new Scanner(System.in).nextLine();
				Object[] enregistrement = null;
				Arbre arbre = arbres[numArbre];
				System.out.println("Parcours de la recherche : ");
				if(isInteger) {
					enregistrement = arbre.rechercherEnregistrement(Integer.parseInt(key), true);
				} else {
					enregistrement = arbre.rechercherEnregistrement(key, true);
				}
				if(enregistrement != null) {
					StringBuffer values = new StringBuffer();
					for (Object o : enregistrement)
						values.append((String)o.toString() + "; ");
					System.out.println("Enregistrement trouvé !");
					System.out.println("Résultat : " + key + " -> " + values + "\n");
				} else {
					System.out.println("Aucune clé correspondante trouvée\n");
				}
			}
			else if(action.equals("rr")) {
				int numArbre = 0;
				if(arbres.length > 1) {
					StringBuffer nums = new StringBuffer("(");
					for(int i = 1; i <= arbres.length; i++) {
						nums.append(i);
						if (i == arbres.length-1) nums.append(", "); else nums.append(")");
					}
					boolean correct = false;
					while(!correct) {
						System.out.println("Recherche à effectuer sur quelle index ? " + nums);
						numArbre = new Scanner(System.in).nextInt();
						if(numArbre > 0 && numArbre <= arbres.length)
							correct = true;
						else
							System.out.println("Valeur incorrecte");
					}
				} else numArbre = 1;
				numArbre--;
				boolean isInteger = donnees.indexIsInterger(numArbre);
				String type = isInteger ? "un entier" : "une chaine de caractère";
				System.out.println("Clé de début ? Veuillez renseigner " + type);
				String keyDebut = new Scanner(System.in).nextLine();
				System.out.println("Clé de fin ? Veuillez renseigner " + type);
				String keyFin = new Scanner(System.in).nextLine();
				TreeMap<?, Object[]> enregistrements;
				Arbre arbre = arbres[numArbre];
				System.out.println("Parcours de la recherche : ");
				if(isInteger) {
					enregistrements = arbre.rechercherEnregistrements(Integer.parseInt(keyDebut), Integer.parseInt(keyFin), true);
				} else {
					enregistrements = arbre.rechercherEnregistrements(keyDebut, keyFin, true);
				}
				if(enregistrements != null && enregistrements.size() > 0) {
					System.out.println("Enregistrement trouvé ! \nParcours des clés sur les feuilles liées...");
					StringBuffer values = new StringBuffer();
					for (Entry<?, Object[]> entry : enregistrements.entrySet()) {
						for (Object o : entry.getValue())
							values.append((String)o.toString() + "; ");
						System.out.println("Résultat : " + entry.getKey() + " -> " + values);
						values = new StringBuffer();
					}
					System.out.println();
				} else {
					System.out.println("La clé recherchée est introuvable\n");
				}
			}
			else if(action.equals("q") || action.equals("quitter")) {
				System.out.print("Bye !");
				stop = true;
			}
			else {
				System.out.println("Action incorrecte\n");
			}
		}
	}
}
