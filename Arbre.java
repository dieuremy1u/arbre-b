import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Arbre B+ composé de noeuds
 * 
 * @author Guylan Dieu-Remy, Thomas Ehrhard
 *
 * @param <E> type des clés des noeuds de l'arbre
 */
public class Arbre<E extends Comparable<E>> {

	/**
	 * Taille maximale de clés dans les noeuds de l'arbre
	 */
	private int n;
	
	/**
	 * Racine de l'arbre
	 */
	private Noeud<E> racine;
	
	/**
	 * Liste des noeuds de l'arbre
	 */
	private ArrayList<Noeud<E>> noeuds;

	/**
	 * Construit la représentation d'un arbre B+
	 * @param n Taille des noeuds
	 */
	public Arbre(int n) {
		this.n = n;
		this.racine = new Noeud<E>();
		this.noeuds = new ArrayList<Noeud<E>>();
		this.noeuds.add(this.racine);
	}

	/**
	 * Recherche un enregistrement selon une clé donnée
	 * @param key clé de recherche
	 * @param afficherParcours vrai si on souhaite afficher le parcours de la recherche en ligne de commande, faux sinon
	 * @return la liste des données de l'enregistrement (sans la clé), null si la clé est introuvable
	 */
	public Object[] rechercherEnregistrement(E key, boolean afficherParcours) {
		Noeud<E> feuille = this.rechercherFeuilleDAcceuil(this.racine, key, afficherParcours);
		return feuille.donneesLiees.get(key);
	}

	/**
	 * Recherche tous les enregistrements entre deux valeurs de clé données
	 * @param keyDebut borne inférieure
	 * @param keyFin borne supérieure
	 * @param afficherParcours vrai si on souhaite afficher le parcours de la recherche en ligne de commande, faux sinon
	 * @return ensemble de clés/valeurs correspondant aux éventuels enregistrements trouvés
	 */
	public TreeMap<E, Object[]> rechercherEnregistrements(E keyDebut, E keyFin, boolean afficherParcours) {
		Noeud<E> feuille = this.rechercherFeuilleDAcceuil(this.racine, keyDebut, afficherParcours);
		TreeMap<E, Object[]> enregistrements = new TreeMap<E, Object[]>();
		E keyCourante = keyDebut;
		boolean stop = false;
		while (!stop) {
			if(feuille.donneesLiees.get(keyCourante) != null) {
				enregistrements.put(keyCourante, feuille.donneesLiees.get(keyCourante));
			}
			keyCourante = feuille.valeurs.higher(keyCourante);
			if(keyCourante == null) {
				if((feuille = feuille.feuilleDroite) != null) {
					keyCourante = feuille.valeurs.first();
				} else {
					stop = true;
				}
			}
			if(!stop && keyCourante.compareTo(keyFin) > 0) {
				stop = true;
			}
		}
		return enregistrements;
	}

	/**
	 * Ajoute un enregistrement dans l'arbre
	 * @param key clé de l'enregistrement
	 * @param values données de l'enregistrement
	 */
	public void ajouterValeur(E key, Object[] values) {
		Noeud<E> feuille = this.rechercherFeuilleDAcceuil(this.racine, key, false);
		feuille.ajouterValeur(key, values);
		this.spliterNoeudDebordant(feuille);
	}

	/**
	 * Supprime un enregistrement de l'arbre
	 * @param key clé de l'enregistrement
	 */
	public void supprimerValeur(E key) {
		Noeud<E> feuille = this.rechercherFeuilleDAcceuil(this.racine, key, false);
		boolean keyIsFirst = feuille.supprimerValeur(key);
		boolean merge = this.mergeNoeuds(feuille, key, keyIsFirst);
		if(!merge && keyIsFirst && !this.racine.valeurs.isEmpty())
			this.remplacerValeur(this.racine, key, feuille.valeurs.first());
	}

	/**
	 * Recherche la feuille qui va accueillir la valeur à ajouter
	 * @param noeud Noeud courant
	 * @param valeurAPlacer Valeur à ajouter
	 * @return Feuille qui accueillera la valeur à ajouter
	 */
	private Noeud<E> rechercherFeuilleDAcceuil(Noeud<E> noeud, E valeurAPlacer, boolean afficherParcours) {
		if(afficherParcours) 
			System.out.print("noeud #" + noeud.id + " -> ");
		if(!noeud.estFeuille) {
			int i = 0;
			for(E valeur : noeud.valeurs) {
				if(valeurAPlacer.compareTo(valeur) < 0) {
					return this.rechercherFeuilleDAcceuil(noeud.pointeurs.get(i), valeurAPlacer, afficherParcours);
				}
				i++;
			}
			return this.rechercherFeuilleDAcceuil(noeud.pointeurs.get(noeud.pointeurs.size() - 1), valeurAPlacer, afficherParcours);
		}
		return noeud;
	}

	/**
	 * Parcours l'arbre et remplace une valeur donnée par une nouvelle valeur (pour la méthode full de la suppression)
	 * @param noeud noeud courant
	 * @param valeurARemplacer valeur à remplacer
	 * @param nouvelleValeur valeur de remplacement
	 */
	private void remplacerValeur(Noeud<E> noeud, E valeurARemplacer, E nouvelleValeur) {
		if(!noeud.estFeuille) {
			if(noeud.valeurs.contains(valeurARemplacer)) {
				noeud.valeurs.remove(valeurARemplacer);
				noeud.valeurs.add(nouvelleValeur);
			}
			int i = 0;
			for(E valeur : noeud.valeurs) {
				if(nouvelleValeur.compareTo(valeur) < 0) {
					this.remplacerValeur(noeud.pointeurs.get(i), valeurARemplacer, nouvelleValeur);
				}
				i++;
			}
			this.remplacerValeur(noeud.pointeurs.get(noeud.pointeurs.size() - 1), valeurARemplacer, nouvelleValeur);
		}
	}

	/**
	 * Split récursivement les éventuelles noeuds qui subissent un débordement suite à un ajout de valeur
	 * @param noeud Noeud à vérifier qui doit éventuellement être splité
	 */
	private void spliterNoeudDebordant(Noeud<E> noeud) {
		if(noeud.valeurs.size() > this.n) {	

			/* initialisation des variables */
			TreeSet<E> valeurs = new TreeSet<E>();
			TreeMap<E, Object[]> donneesLiees = null;	
			if(noeud.estFeuille) {
				donneesLiees = new TreeMap<E, Object[]>();
			}
			int nbElem = noeud.valeurs.size();

			/* split : construction du nouveau noeud voisin */
			for(int i = nbElem; i > nbElem/2; i--) {
				E v = noeud.valeurs.pollLast();
				valeurs.add(v);
				if(noeud.estFeuille) {
					donneesLiees.put(v, noeud.donneesLiees.get(v));
					noeud.donneesLiees.remove(v);
				}
			}
			nbElem = noeud.pointeurs.size()-1;
			ArrayList<Noeud<E>> pointeurs = new ArrayList<Noeud<E>>();
			for(int i = nbElem; i > (nbElem/2); i--) {
				pointeurs.add(noeud.pointeurs.remove(i));
			}
			Noeud<E> nouveauNoeud = null;
			if(noeud.estFeuille) {
				nouveauNoeud = new Noeud<E>(noeud.niveau, valeurs, pointeurs, noeud.parent, donneesLiees);
				Noeud<E> tmp = noeud.feuilleDroite;
				noeud.feuilleDroite = nouveauNoeud;
				nouveauNoeud.feuilleDroite = tmp;
			}
			else
				nouveauNoeud = new Noeud<E>(noeud.niveau, valeurs, pointeurs, noeud.parent);
			for (Noeud<E> n : nouveauNoeud.pointeurs)
				n.parent = nouveauNoeud;
			this.noeuds.add(nouveauNoeud);

			/* suppression (ou non) de la valeur médiane dans le nouveau noeud */
			E valeurMediane = nouveauNoeud.valeurs.first();
			if(nouveauNoeud.estIntermediaire) 
				nouveauNoeud.valeurs.pollFirst();

			/* cas racine : création d'une nouvelle racine */
			if(noeud.estRacine) { 
				valeurs = new TreeSet<E>();
				valeurs.add(valeurMediane);
				pointeurs = new ArrayList<Noeud<E>>();
				pointeurs.add(noeud);
				pointeurs.add(nouveauNoeud);
				Noeud<E> nouvelleRacine = new Noeud<E>(valeurs, pointeurs);
				this.racine = nouvelleRacine;
				this.noeuds.add(this.racine);
				noeud.estRacine = nouveauNoeud.estRacine;
				noeud.estFeuille = nouveauNoeud.estFeuille;
				noeud.estIntermediaire = nouveauNoeud.estIntermediaire;
				noeud.parent = this.racine;
				nouveauNoeud.parent = this.racine;
				this.mettreAJourLesNiveaux(this.racine);
			}

			/*  autres cas : remontée de la valeur médiane vers parent */
			else { 
				nouveauNoeud.parent.ajouterValeur(valeurMediane, null);
				nouveauNoeud.parent.ajouterPointeur(nouveauNoeud);
				this.spliterNoeudDebordant(nouveauNoeud.parent); // recursivité
			}
		}
	}

	/**
	 * Merge récursivement les éventuels noeuds qui subissent un une insuffisance de valeurs à la suite d'une suppression
	 * @param noeud Noeud qui doit éventuellement être mergé
	 * @param valeurSupprimee valeur qui a été supprimée de l'arbre
	 * @param keyIsFirst vrai si la valeur a supprimer est la première valeur de son noeud, faux sinon
	 * @return vrai si un merge a eu lieu, faux sinon
	 */
	private boolean mergeNoeuds(Noeud<E> noeud, E valeurSupprimee, boolean keyIsFirst) {
		if(((noeud.estIntermediaire && noeud.valeurs.size() < ((this.n+1)/2)-1) || (noeud.estFeuille && noeud.valeurs.size() < (this.n+1)/2)) && !noeud.estRacine) {	

			/* initialisation des variables */
			Noeud<E> voisin = noeud.getVoisinDroite();
			boolean isVoisinDroite = true;
			if(voisin == null) {
				voisin = noeud.getVoisinGauche();
				isVoisinDroite = false;
			}

			/* le noeud courant récupère une valeur de son voisin */
			E valeurMergee;
			if(isVoisinDroite)
				valeurMergee = voisin.valeurs.pollFirst();
			else
				valeurMergee = voisin.valeurs.pollLast();
			noeud.valeurs.add(valeurMergee);
			if(noeud.estFeuille) {
				Entry<E, Object[]> entry;
				if(isVoisinDroite)
					entry = voisin.donneesLiees.pollFirstEntry();
				else 
					entry = voisin.donneesLiees.pollLastEntry();
				noeud.donneesLiees.put(entry.getKey(), entry.getValue());
			} else {
				Noeud<E> pointeur;
				if(isVoisinDroite)
					pointeur = voisin.pointeurs.remove(0);
				else
					pointeur = voisin.pointeurs.remove(voisin.pointeurs.size() - 1);
				noeud.ajouterPointeur(pointeur);
				if(isVoisinDroite) {
					while(!pointeur.estFeuille)
						pointeur = pointeur.pointeurs.get(0);
					noeud.valeurs.add(pointeur.valeurs.first());
				}
				else {
					pointeur = noeud.pointeurs.get(1);
					while(!pointeur.estFeuille) 
						pointeur = pointeur.pointeurs.get(1);
					noeud.valeurs.add(pointeur.valeurs.first());
				}
			}

			/* si son voisin obtient une insuffisance de valeur : on merge completement les 2 noeuds */
			if((voisin.estIntermediaire && voisin.valeurs.size() < ((this.n+1)/2)-1) || (voisin.estFeuille && voisin.valeurs.size() < (this.n+1)/2)) {

				/* suppression d'une valeur dans le noeud parent */
				if(!noeud.parent.estRacine || noeud.parent.valeurs.size() > 1) {
					if(isVoisinDroite)
						noeud.parent.valeurs.remove(valeurMergee);
					else
						noeud.parent.valeurs.remove(noeud.valeurs.higher(noeud.valeurs.first()));
				}

				/* merge complet avec le noeud voisin */
				int j = voisin.valeurs.size();
				for(int i = 0; i < j; i++) {
					noeud.valeurs.add(voisin.valeurs.pollFirst());
					if(noeud.estFeuille) {
						Entry<E, Object[]> entry = voisin.donneesLiees.pollFirstEntry();
						noeud.donneesLiees.put(entry.getKey(), entry.getValue());
					} else {
						Noeud<E> pointeur = voisin.pointeurs.remove(0);
						noeud.ajouterPointeur(pointeur);
					}
				}
				this.noeuds.remove(voisin);
				noeud.parent.supprimerPointeur(voisin);
				if(noeud.estFeuille) {
					if(isVoisinDroite)
						noeud.feuilleDroite = voisin.feuilleDroite;
					else if(noeud.parent.pointeurs.size() >= 2)
						noeud.getVoisinGauche().feuilleDroite = noeud;

				} else {
					Noeud<E> pointeur = voisin.pointeurs.remove(0);
					noeud.ajouterPointeur(pointeur);
				}
				for (Noeud<E> enfant : noeud.pointeurs) 
					enfant.parent = noeud;
				if(keyIsFirst)
					this.remplacerValeur(this.racine, valeurSupprimee, noeud.valeurs.first());

				/* si la racine doit être remplacée */
				if(noeud.parent.estRacine) {
					if(noeud.parent.pointeurs.size() < 2) {
						noeud.estRacine = true;
						noeud.estIntermediaire = false;
						this.noeuds.remove(noeud.parent);
						this.racine = noeud;
						this.racine.feuilleDroite = null;
						this.racine.parent = null;
						this.racine.niveau = 0;
						this.mettreAJourLesNiveaux(this.racine);
					}
				} else 
					this.mergeNoeuds(noeud.parent, valeurSupprimee, keyIsFirst); // recursivité
			} else {
				/* stratégie full : met à jour les valeurs supprimées/déplacées dans les noeuds parents */
				if(keyIsFirst)
					this.remplacerValeur(this.racine, valeurSupprimee, noeud.valeurs.first());
				else if(isVoisinDroite)
					this.remplacerValeur(this.racine, valeurMergee, voisin.valeurs.first());
				else
					this.remplacerValeur(this.racine, noeud.valeurs.higher(noeud.valeurs.first()), noeud.valeurs.first());
			}
			return true;
		}
		return false;
	}

	/**
	 * Met à jour le niveau de chaque noeud de l'arbre
	 * @param noeud Noeud de départ
	 */
	private void mettreAJourLesNiveaux(Noeud<E> noeud) {
		for (Noeud<E> n : noeud.pointeurs) {
			n.niveau = noeud.niveau+1;
			this.mettreAJourLesNiveaux(n);
		}
	}

	/**
	 * Calcule le taux de remplissage de l'arbre
	 * @return taux de remplissage de l'arbre
	 */
	public int calculerTauxRemplissage() {
		double nbNoeuds = this.noeuds.size();
		double nbValeurs = 0;
		for (Noeud<E> n : this.noeuds) {
			nbValeurs += n.valeurs.size();
		}
		return (int)(nbValeurs/(nbNoeuds*this.n)*100);
	}

	/**
	 * Affichage récursif des noeuds de l'arbre
	 * @param noeud noeud courant
	 * @return chaine de caractère correspondant à la représentation de l'arbre en ligne de commande
	 */
	private String toString(Noeud<E> noeud) {
		StringBuffer res = new StringBuffer();
		res.append(noeud);
		if(noeud.pointeurs.size() != 0) {
			for(Noeud<E> n : noeud.pointeurs)
				res.append(this.toString(n));
		}
		return res.toString();
	}

	/**
	 * Affichage par défaut de l'arbre
	 * @return chaine de caractère correspondant à la représentation de l'arbre en ligne de commande ainsi que son taux de remplissage
	 */
	@Override
	public String toString() {
		return "\n" + this.toString(this.racine) + "\nTaux de remplissage : " + this.calculerTauxRemplissage() + "%\n";
	}

}
