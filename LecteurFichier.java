import java.io.*;
import java.util.ArrayList;

/**
 * Lecteur de fichier qui récupère les données depuis un fichier
 * 
 * @author Guylan Dieu-Remy, Thomas Ehrhard
 *
 */
public class LecteurFichier {

	/**
	 * Lecteur
	 */
	private FileReader fileReader;
	
	/**
	 * Nom du fichier
	 */
	protected String nomFichier;

	/**
	 * Construit un lecteur à partir d'un fichier
	 * @param fichier lien du fichier
	 */
	public LecteurFichier(String fichier) {
		this.nomFichier = fichier;
		try {
			this.fileReader = new FileReader(fichier);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lit le fichier et construit une structure de données permettant la manipulation des données
	 * @return données structurées
	 */
	public Donnees getDonnees() {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try(BufferedReader br = new BufferedReader(this.fileReader)) {
			for(String line; (line = br.readLine()) != null; ) {
				String[] dataLine = line.split("\\|");
				list.add(dataLine);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Donnees data = new Donnees(list, this.nomFichier);
		return data;
	}
}
