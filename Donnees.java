import java.util.ArrayList;
import java.util.HashMap;

/**
 * Structure les données et permet de les manipuler simplement
 * 
 * @author Guylan Dieu-Remy, Thomas Ehrhard
 *
 */
public class Donnees {

	/**
	 * Données structurer
	 */
	private ArrayList<String[]> data;
	
	/**
	 * Nom du fichier si les données proviennent d'un fichier, faux sinon
	 */
	protected String nomFichier;

	/**
	 * Constructeur Donnees sans fichier
	 * @param data données
	 */
	public Donnees(ArrayList<String[]> data) {
		this.data = data;
		this.nomFichier = null;
	}

	/**
	 * Constructeur Donnees avec fichier
	 * @param data données
	 * @param fichier nom du fichier source
	 */
	Donnees(ArrayList<String[]> data, String fichier) {
		this.data = data;
		this.nomFichier = fichier;
	}

	/**
	 * Construit les données indexées sur le numéro de colonne renseigné
	 * @param i numéro de colonne
	 * @return liste de clé/valeurs
	 */
	public HashMap<String, Object[]> getDonneesIndexees(int i) {
		HashMap<String, Object[]> donneesIndexee = new HashMap<String, Object[]>();
		ArrayList<String[]> data = this.data;
		for (String[] enregistrement : data) {
			String[] values = new String[this.getNbColonnes()-1];
			int k = 0;
			for(int j = 0; j < enregistrement.length; j++) {
				if(i != j)
					values[k++] = enregistrement[j];
			}
			donneesIndexee.put(enregistrement[i], values);
		}
		return donneesIndexee;
	}

	/**
	 * Donne le nombre de colonnes total
	 * @return nombre de colonnes
	 */
	public int getNbColonnes() {
		return this.data.get(0).length;
	}

	/**
	 * Test si la clé est un entier
	 * @param i numéro de la clé
	 * @return vrai si clé est un entier, faux sinon
	 */
	public boolean indexIsInterger(int i) {
		return this.isInteger(this.data.get(0)[i]);
	}

	/**
	 * Test si la chaine de caractère peut correspondre à un entier
	 * @param str chaine de caractère
	 * @return vrai si la chaine peut correspondre à un entier, faux sinon
	 */
	private boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}
}
